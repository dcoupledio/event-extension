<?php namespace Decoupled\Core\Extension\Event;

use Decoupled\Core\Event\EventDelegator;
use Decoupled\Core\Event\EventFactory;
use Decoupled\Core\Event\EventDispatcher;
use Decoupled\Core\Event\EventListenerFactory;
use Decoupled\Core\Event\Event;
use Decoupled\Core\Application\ApplicationExtension;

class EventExtension extends ApplicationExtension{

    public function getName()
    {
        return 'event.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app['$event.factory'] = function(){

            return new EventFactory(); 
        };  

        $app['$event.listener.factory'] = function( $container ){

            $factory = new EventListenerFactory();

            $factory->setActionFactory( $container['$action.factory'] );

            return $factory;
        };

        $app['$event.dispatcher.default'] = function( $container ){

            return new EventDispatcher();
        };

        $app['$event.delegator'] = function( $container ){
    
            $del = new EventDelegator();

            $del->setListenerFactory( $container['$event.listener.factory'] );

            $del->setEventFactory( $container['$event.factory'] );

            $del->addDispatcher( 'event.default', $container['$event.dispatcher.default'] );

            return $del;
        };

        $app->setMethod('when', function() use($app){

            $when = [ $app['$event.delegator'], 'when' ];

            $params = func_get_args();

            return call_user_func_array( $when, $params );
        });

        $app->setMethod('dispatch', function() use($app){

            $dispatch = [ $app['$event.delegator'], 'dispatch' ];

            $params = func_get_args();

            return call_user_func_array( $dispatch, $params );
        });
    }
}