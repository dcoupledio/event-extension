<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Extension\Event\EventExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Action\ActionExtension;

class EventExtTest extends TestCase{

    public function testEventExtensionCanBeUsed()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new EventExtension() );

        return $app;
    }

    /**
    * @depends testEventExtensionCanBeUsed
    **/

    public function testCanUseDelegatorFromApp( $app )
    {
        $app['$event.delegator']->when( 'foo_event' )->uses(function( $event, $foo ){

            $this->assertEquals( $foo, 1 );
        });

        $app['$event.delegator']->dispatch( 'foo_event', [ 'foo' => 1 ] );
    }

    /**
     * @depends testEventExtensionCanBeUsed
     */

    public function testCanDelegateWithMagicMethod( $app )
    {
        $app->when( 'bar_event' )->uses(function( $bar ){

            $this->assertEquals( $bar, 1 );
        });

        $app->dispatch( 'bar_event', [ 'bar' => 1 ] );
    }

}